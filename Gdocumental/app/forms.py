from django import forms

class Prueba(forms.Form):
    nombre = forms.CharField(max_length=50,widget=forms.TextInput,required=True)
    email = forms.EmailField(max_length=60,widget=forms.TextInput,required=True)
    mensaje = forms.CharField(max_length=400,widget=forms.Textarea,required=True)