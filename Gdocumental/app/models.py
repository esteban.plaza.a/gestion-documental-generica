from django.db import models
from django.contrib import admin
from datetime import datetime,date,time,timedelta




from django.contrib.auth.models import AbstractBaseUser,PermissionsMixin,BaseUserManager


class PersonalizadoBaseManager(BaseUserManager):
    def create_user(self,usuario,password):
        user=self.model(usuario=usuario)
        user.set_password(password)
        user.is_staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self,usuario,password):
        user=self.create_user(usuario,password)
        user.is_staff=True
        user.tipo=(0)
        user.is_superuser=True
        user.save(using=self._db)
        return user



class Usuario(AbstractBaseUser,PermissionsMixin,):
    listaTipos = (
        ('0','Operario'),
        ('1','Supervisor'),
        ('2','Administrador')
    )
    usuario = models.CharField(max_length=50, unique=True,verbose_name='Nombre')
    tipo=models.CharField(max_length=1, choices=listaTipos,verbose_name='Tipo de Actor')
    is_active = models.BooleanField(default=True,verbose_name='Esta Activo?')
    is_staff=models.BooleanField(default=False,verbose_name='Es Adminsitrador?')
    correo= models.EmailField ( max_length=100, verbose_name='Correo')
    USERNAME_FIELD='usuario'
    object=PersonalizadoBaseManager()
    def __str__(self):
       return  self.usuario





#listo
class tipo_documento(models.Model):
    nombre_tdocumento = models.CharField(max_length=50, verbose_name='Nombre del Tipo de Documento')
    class Meta:
        verbose_name = 'Tipo de Documento'
        verbose_name_plural ='Tipos de Documentos'
    def __str__(self):
       return  self.nombre_tdocumento

class estado_documento(models.Model):
    nombre_estado=models.CharField(max_length=20, verbose_name='Nombre del Estado de Documento')
    class Meta:
        verbose_name = 'Estado de Documento'
        verbose_name_plural = 'Estados de Documentos'
    def __str__(self):
       return  self.nombre_estado

#listo
class proceso(models.Model):
    nombre_proceso=models.CharField(max_length=50,verbose_name='Nombre de Proceso')
    class Meta:
        verbose_name = 'Proceso'
        verbose_name_plural = verbose_name+'s'
    def __str__(self):
       return  self.nombre_proceso

#listo
class organizacion(models.Model):
    nombre_organizacion= models.CharField(max_length=60, verbose_name='Nombre de Organizacion')
    direccion_organizacion= models.CharField(max_length=60, verbose_name='Direccion de Organizacion')
    class Meta:
        verbose_name = 'Organizacion'
        verbose_name_plural = verbose_name+'es'
    def __str__(self):
       return  self.nombre_organizacion+ "-" + self.direccion_organizacion

#listo
class area_organizacion(models.Model):
    id_organizacion= models.ForeignKey(organizacion,on_delete=models.CASCADE, verbose_name='Id de Organizacion')
    nombre_area_organi=models.CharField(max_length=50, verbose_name='Nombre de Area de Organizacion')
    class Meta:
        verbose_name = 'Area de Organizacion'
        verbose_name_plural = 'Areas de Organizacion'
    def __str__(self):
       return  self.nombre_area_organi

#clase pseudo abstracta, dado que no se puede colocar como abstracta de verdad
""" deprecated
class actor(models.Model):
    id_area_organi=models.ForeignKey(area_organizacion,on_delete=models.CASCADE, verbose_name='Id de Area de Organizacion')
    nombre_actor=models.CharField(max_length=30, verbose_name='Nombres')
    apellido_actor=models.CharField(max_length=30,verbose_name='Apellidos')
    rut_actor=models.CharField(max_length=10, verbose_name='Rut')
    correo_actor=models.EmailField(max_length=100, verbose_name='Correo')
    class Meta:
        verbose_name = 'Actor'
        verbose_name_plural = verbose_name+'es'
"""

#listo
class tipo_flujo(models.Model):
    nombre_tflujo = models.CharField(max_length=50, verbose_name='Nombre')
    class Meta:
        verbose_name = 'Tipo de Flujo'
        verbose_name_plural = 'Tipos de Flujos'
    def __str__(self):
       return  self.nombre_tflujo


#listo
class flujo(models.Model):
    id_proceso=models.ForeignKey(proceso,on_delete=models.CASCADE,verbose_name='Id de Proceso')
    id_usuario=models.ForeignKey(Usuario,on_delete=models.CASCADE,verbose_name='Id Actor')
    id_tflujo=models.ForeignKey(tipo_flujo,on_delete=models.CASCADE, verbose_name='Id de Tipo de Flujo')
    fechaini_flujo=models.DateTimeField(verbose_name='Fecha de Inicio ')
    fechaterm_flujo=models.DateTimeField(verbose_name='Fecha de Termino')
    class Meta:
        verbose_name = 'Flujo'
        verbose_name_plural = verbose_name+'s'
    def __str__(self):
       return  self.id_tflujo


#listo
class documento(models.Model):
    nombre_documento = models.CharField(max_length=50, verbose_name='Nombre de Documento')
    link_documento = models.FileField(verbose_name='Subir Documento')
    id_estado=models.ForeignKey(tipo_documento,on_delete=models.CASCADE,verbose_name='Id Estado')
    class Meta:
        verbose_name = 'Documento'
        verbose_name_plural = verbose_name+'s'
    def __str__(self):
       return  self.nombre_documento

#listo
class tipo_accion(models.Model):
    nombre_tipo_accion = models.CharField(max_length=256, verbose_name='Nombre de tipo de accion')
    class Meta:
        verbose_name = 'Tipo de accion'
        verbose_name_plural = 'Tipos de acciones'
    def __str__(self):
       return  self.nombre_tipo_accion

#listo
class auditoria(models.Model):
    id_tipo_accion = models.ForeignKey(tipo_accion,on_delete=models.CASCADE,verbose_name='Id de accion')
    tipo_accion = models.CharField(max_length=50, verbose_name='Numero de tipo de accion')
    involucrado = models.CharField(max_length=256, verbose_name='Nombre usuario involucrado')
    fecha = datetime.now()
    auditado = models.CharField(max_length=15, verbose_name='Verificacion de estado')
    class Meta:
        verbose_name = 'Trazabilidad'
        verbose_name_plural = 'Trazabilidades'


