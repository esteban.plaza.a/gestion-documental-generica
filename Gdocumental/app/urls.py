from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
from django.urls import path,include
from django.contrib import admin
from django.conf.urls import include, url


urlpatterns = [
    path('', views.home, name='home'),
    path ( 'login/', auth_views.LoginView.as_view ( template_name='app/login.html' )),
    url( 'panel/', views.panel),
    url('operador/', views.operador, name='operador'),
    url('supervisor/', views.supervisor, name='supervisor')

]
