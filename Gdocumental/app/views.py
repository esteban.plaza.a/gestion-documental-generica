from django.shortcuts import render
from django.contrib.auth.forms import  UserCreationForm, AuthenticationForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect


app_name='app'

#vistas
def home(request):
    return render(request, 'app/index.html')


@login_required
def operador(request):
    return render(request, 'app/operador.html')


@login_required
def supervisor(request):
    return render(request, 'app/supervisor.html')

def loginview(request):
    if request.method=='POST':
        pass
    else:
        form =  AuthenticationForm()
    return render(request,"app/login.html",{'form':form})


"""
cuando el tipo de usuario es 
0=admin
1=supervisor
3=operador
"""
@login_required
def panel(request):
    tipo_user=request.user.tipo
    link=""
    if tipo_user=='2':
        link="/operador"
    elif tipo_user=='1':
        link = "/supervisor"
    else:
        tipo_user = "/admin"
    return redirect(link)
